export default class Model {
    constructor(){
        this.view = null;
        this.todos = JSON.parse(localStorage.getItem('todos'));
        this.auxFiltered = [];

        if(!this.todos || this.todos.length === 0){
            this.currentId = 1;
            this.todos = [];
        }else{
            this.currentId = this.todos[this.todos.length - 1].id + 1;
        }
    }

    setView(view){
        this.view = view;
    }

    getTodos(){
        return this.todos.map( t => ({...t}));
    }

    addTodo(title,description){
        const todo = {
            id: this.currentId++,
            title,
            description,
            completed: false,
        };

        this.todos.push(todo);
        this.save();

        /*Se crea un nuevo objeto que cuenta con todas las propiedades
        lo que va a hacer es que va a arrastrar todo el arreglo en un nuevo objeto */
        return {...todo};
    }

    editTodo(id,data){
        const index = this.findTodo(id);
        Object.assign(this.todos[index], data);
        this.save();
    }

    findTodo(id){
        return this.todos.findIndex(t => t.id === id);
    }

    removeTodo(id){
        const index = this.findTodo(id);
        this.todos.splice(index,1);
        this.save();
    }
        

    save(){
        localStorage.setItem('todos', JSON.stringify(this.todos));
    }

    lookDescTitle(todo, searchText){
        if(
            todo.description.toLowerCase().includes(searchText) ||
            todo.title.toLowerCase().includes(searchText)
        ){
            return todo;
        }
    }

    searchRows(filterValue,searchText){
        let todosFiltered = this.todos.slice();
        
        if(!searchText){
            return;
        }else{
            this.auxFiltered = []
            switch (filterValue){
                case 'all':
                    this.auxFiltered = todosFiltered.filter(todo => this.lookDescTitle(todo,searchText));                    
                    break;
                case 'completed':
                    todosFiltered = this.todos.filter(todo => todo.completed);
                    this.auxFiltered = todosFiltered.filter(todo => this.lookDescTitle(todo,searchText)); 
                    
                    break;
                case 'uncompleted':
                    todosFiltered = this.todos.filter(todo => !todo.completed);
                    this.auxFiltered = todosFiltered.filter(todo => this.lookDescTitle(todo,searchText));                 
                    break;
            }
        }
        return this.auxFiltered;
    }

    toogleCompleted(id){
        const index = this.findTodo(id);
        const todo = this.todos[index];
        todo.completed = !todo.completed;
        this.save();
    }
}
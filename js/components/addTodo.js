import Alert from './alert.js';

export default class AddTodo{
    constructor(){
        /*En el constructor es donde vamos a crear las variables requeridas */
        this.addButton = document.getElementById('add');
        this.titleInput = document.getElementById('title');
        this.descriptionInput = document.getElementById('description');
        this.alert = new Alert('alert');
    }

   
    onClick(callback){
        this.addButton.onclick = () =>{
            if(
                this.titleInput.value === '' ||
                this.descriptionInput === ''
            ){
                this.alert.show('You should write something in all fields!');
                return;
            }

            this.alert.hide();
            callback( this.titleInput.value, this.descriptionInput.value );
        };
    }
}
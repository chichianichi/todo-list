import Alert from './alert.js';

export default class Search{
  constructor(){
      this.allFilter = document.getElementById('radioAll');
      this.completedFilter = document.getElementById('radioCompleted');
      this.uncompletedFilter = document.getElementById('radioUncompleted');
      this.searchInput = document.getElementById('searchInput'); 
      this.searchButton = document.getElementById('search'); 
      this.alert = new Alert('alert');
  }
  
  onClick(callback){
    this.searchButton.onclick = () => {
      let filterValue;
      const searchText = this.searchInput.value.toLowerCase(); 
      
      if(
        (searchText === "")  ||
        !(
          this.allFilter.checked ||
          this.completedFilter.checked ||
          this.uncompletedFilter.checked
        )
      ){
        this.alert.show('You should check one filter option and write something in the input!');
        return;
      }else{
        this.alert.hide();
        
        if(this.allFilter.checked){
          filterValue = 'all';
        }else if(this.completedFilter.checked){
          filterValue = 'completed';
        }else if(this.uncompletedFilter.checked){
          filterValue = 'uncompleted';
        }
        
        callback(filterValue,searchText);
      }
    }
  }
}
/*En el index.js es donde vamos a invocar a todas las clases */
import Model from './model.js';
import View from './view.js';

const model = new Model();
const view = new View();

/*Aqui es donde vamos a conectar los modulos, dependiendo
de cuales interactuan entre sí */
model.setView(view);
view.setModel(model);
view.render();